ActiveAdmin.register Author do
  permit_params :name, :last_name

  menu parent: 'Answers'

  index do
    selectable_column
    id_column
    column :name
    column :last_name
    column :avatar_url
    column :fb_token
    actions
  end

  filter :name
  filter :last_name

  form do |f|
    f.inputs "Author Details" do
      f.input :name
      f.input :last_name
    end
    f.actions
  end

end
