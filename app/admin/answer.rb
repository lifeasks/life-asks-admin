ActiveAdmin.register Answer do
  permit_params :text

  index do
    selectable_column
    id_column
    column :text
    column 'Author', sortable: :author_id do |answer|
      if answer.author_id
        "#{Author.find(answer.author_id).name} #{Author.find(answer.author_id).last_name}"
      else
        "Anonymus"
      end
    end
    column 'Question', sortable: :question_id do |answer|
      if answer.question_id
        "#{Question.find(answer.question_id).text}"
      else
        "None"
      end
    end
    column :published
    column :likes

    actions
  end

  filter :text

  form do |f|
    f.inputs "Answer Details" do
      f.input :text
      f.input :published
      f.input :author_id, label: 'Author', collection: Author.all
      f.input :question_id, label: 'Question', collection: Question.all
    end
    f.actions
  end

end
