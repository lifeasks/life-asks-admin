ActiveAdmin.register Question do
  permit_params :text, :featured, :visible, :category

  index do
    selectable_column
    id_column
    column :text
    column 'Category', sortable: :category_id do |question|
      Category.find(question.category_id).name if question.category_id
    end
    column :featured
    column :visible
    actions
  end

  filter :text

  form do |f|
    f.inputs "Question Details" do
      f.input :text
      f.input :category, label: 'Category', collection: Category.all
      f.input :featured
      f.input :visible
    end
    f.actions
  end

  show do
    panel "Details" do
      attributes_table_for question do
        row :text
        row 'Category', sortable: :category_id do |question|
          Category.find(question.category_id).name if question.category_id
        end
        row :featured
        row :visible
      end
    end

    panel "Answers" do
      paginated_collection(question.answers.per_page_kaminari(15), download_links: false) do
        table_for question.answers do
          column :text
          column 'Author', sortable: :author_id do |answer|
            if answer.author_id
              "#{Author.find(answer.author_id).name} #{Author.find(answer.author_id).last_name}"
            else
              "Anonymus"
            end
          end
          column :published
          column :likes
        end
      end
    end
  end
end
