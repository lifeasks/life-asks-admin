class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :text, :featured, :likes, :visible, :category_id, :created_at, :updated_at
  has_many :answers
end
