class AnswerSerializer < ActiveModel::Serializer
  attributes :id, :text, :likes, :published, :question_id, :author_id, :created_at, :updated_at
end
