class Author < ApplicationRecord
  validates :name, :last_name, presence: true

  has_many :answers
end
