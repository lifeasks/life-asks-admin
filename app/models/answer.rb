class Answer < ApplicationRecord
  validates :text, presence: true

  belongs_to :question
  belongs_to :author
end
