class Question < ApplicationRecord
  validates :text, presence: true

  belongs_to :category

  has_many :answers
end
