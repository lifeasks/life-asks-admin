# Installation setup:

1. Get PostgresSQL, info can be [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-14-04)

2. Get RVM (unless you already got it), info in [here](https://rvm.io/rvm/install)

3. Install the correct Ruby version (currently 2.4.1) -> `rvm install ruby-2.4.1`

4. Install the Bundler gem -> `gem install bundler`

5. Install the other gems -> `bundle` | `bundle install`

6. Setup the database.yml file (There's a guide -> `databse.yml.example`).

7. Setup the database -> `rake db:setup`

8. Run the server -> `rails s`

Run `rake db:seed` to create the sample admin user

Run tests:

1. `rake db:test:prepare`

2. `rspec`

# RESTful API endpoints:

#### Get all questions

`/questions`

Response:

```javascript
[
  {
    "id": 1,
    "text": "question 1",
    "featured": null,
    "likes": 0,
    "visible": true,
    "category_id": 1,
    "created_at": "2017-09-06T16:41:36.550Z",
    "updated_at": "2017-09-06T16:41:36.550Z",
    "answers": [
      {
        "id": 1,
        "text": "answer 1",
        "likes": 2,
        "published": true,
        "question_id": 1,
        "author_id": null,
        "created_at": "2017-09-14T15:45:23.463Z",
        "updated_at": "2017-09-14T15:45:23.463Z"
      },
      ...
    ]
  },
  ...
]
```
