source 'https://rubygems.org'

ruby '2.4.1'

# Framework
gem 'rails', '~> 5.1', '>= 5.1.3'

# Databases
gem 'pg', '~> 0.21.0'

# Assets
gem 'uglifier'

# Misc
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'annotate', '>=2.6.0'
gem 'haml', '~> 5.0.0.beta.2'
gem 'haml-rails', '~> 0.9.0'

# Utilities
gem 'devise'
gem 'activeadmin'
gem 'active_skin'
gem 'faker'
gem 'rails_12factor'
gem 'kaminari'

# Active Model Serializers
gem 'active_model_serializers', '~> 0.10.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'pry'
  gem 'pry-remote'
  gem 'dotenv-rails'
  gem 'rspec-rails'
  gem 'factory_girl_rails'

  gem 'shoulda-matchers'
  gem 'database_cleaner'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  gem 'simplecov'
  gem 'capybara'
end
