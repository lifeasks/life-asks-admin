require 'rails_helper'

describe Author do
  describe 'associations' do
    it { should have_many(:answers) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:last_name) }
  end
end