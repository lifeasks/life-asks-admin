require 'rails_helper'

describe Question do
  describe 'associations' do
    it { should have_many(:answers) }
    it { should belong_to(:category) }
  end

  describe 'validations' do
    it { should validate_presence_of(:text) }
  end
end