require 'rails_helper'

describe Answer do
  describe 'associations' do
    it { should belong_to(:question) }
    it { should belong_to(:author)}
  end

  describe 'validations' do
    it { should validate_presence_of(:text) }
  end
end
