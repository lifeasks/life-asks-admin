require 'rails_helper'

RSpec.describe QuestionsController, type: :controller do
  describe 'GET #index' do
    let!(:questions) { create_list(:question, 10) }

    it 'returns questions' do
      get :index

      expect(json_response.count).to eq(questions.count)
    end

    it 'returns status code 200' do
      get :index

      expect(response).to have_http_status(200)
    end
  end
end
