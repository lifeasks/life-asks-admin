FactoryGirl.define do
  factory :question do
    text { Faker::Lorem.sentence }
    featured { Faker::Boolean.boolean }
    likes { Faker::Number.between(1, 100) }
    visible { Faker::Boolean.boolean }
    category
  end
end
