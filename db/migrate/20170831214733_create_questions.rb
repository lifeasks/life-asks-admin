class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.string :question_text
      t.boolean :featured
      t.integer :likes, default: 0
      t.boolean :is_visible, default: true

      t.timestamps
    end
  end
end
