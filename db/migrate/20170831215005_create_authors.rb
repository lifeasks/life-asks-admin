class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :last_name
      t.string :avatar_url
      t.string :fb_token

      t.timestamps
    end
  end
end
