class RenameIsVisibleToVisible < ActiveRecord::Migration[5.1]
  def change
    rename_column :questions, :is_visible, :visible
  end
end
