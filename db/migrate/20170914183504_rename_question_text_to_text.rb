class RenameQuestionTextToText < ActiveRecord::Migration[5.1]
  def change
    rename_column :questions, :question_text, :text
  end
end
