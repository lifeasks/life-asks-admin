class AddAuthorRefsToAnswer < ActiveRecord::Migration[5.1]
  def change
    add_reference :answers, :author, foreign_key: true
  end
end
