class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.text :text
      t.integer :likes, default: 0
      t.boolean :published, defualt: true

      t.timestamps
    end
  end
end
